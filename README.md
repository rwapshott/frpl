__Summary__: A tiny library for enabling the most basic form of reactive programming.
Objects (or signals) can be added to the Signals instance and listeners can be
registered to respond to specific classes of object added.

__Usage__

The Signals service will process objects such that listeners respond in separate
threads, thus creating an asynchronous framework where the caller can add objects
without worrying about the detail of when the listener will be triggered.

Objects that are added to the Signals service might represent and might represent:

- User input
- Files on a disk
- Packets on a socket

Each listener that is registered to receive the signal can then generate subsequent
signals into the framework. Thus allowing the composition of arbitrary complex processing
chains.

In addition, listeners can be registered against a class or sub-class of an object
to be added to the service. This allows multiple listeners to respond to a given
 object, or further complex processing chains.

If the caller generates too many objects to add to the Signals service, their call will
be throttled until there is space to add more objects. This ensures the framework will
not exceed available memory.

__Shutdown__

Importantly, once the program can be certain that no further signals need to be
responded to other than those already in the task queue, then the
Signals.shutdownWhenComplete() method should be called to shutdown this service.

__Releases__

- *1.0* - Initial version, supports Signals and SignalListeners
- *1.1* - Support for object hierarchy
- *1.2* - Support for some slightly more sophisticated Signals (Repeat/Delay)
- *1.3* - Catch unhandled exceptions from the SignalListeners
- *1.4* - Support for throttling the caller when Signals is saturated