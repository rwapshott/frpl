package uk.co.gencoreoperative.frp;

/**
 * A demonstration class for the FRP framework.
 */
public class HelloWorld {
    public static void main(String... args) {
        Signals signals = new Signals();

        signals.register(String.class, new SignalListener<String>() {
            public void process(String s, Signals signals) {
                System.out.println(s);
            }
        });

        signals.register(Integer.class, new SignalListener<Integer>() {
            public void process(Integer integer, Signals signals) {
                signals.add("Hello World " + integer);
                if (integer < 100) {
                    signals.add(integer + 1);
                } else {
                    signals.shutdownWhenComplete();
                }
            }
        });

        signals.add(1);
    }
}
