package uk.co.gencoreoperative.frp;

/**
 * Demonstrates the shutdown function.
 */
public class ShutdownWorld {
    public static void main(String... args) {
        Signals signals = new Signals();
        signals.register(Integer.class, new SignalListener<Integer>() {
            public void process(Integer integer, Signals signals) {
                System.out.println(integer);
            }
        });
        signals.register(String.class, new SignalListener<String>() {
            public void process(String s, Signals signals) {
                signals.shutdownWhenComplete();
            }
        });

        signals.add("");
        for (int ii = 0; ii < 1000; ii++) {
            signals.add(ii);
        }
    }
}
