/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2014 ForgeRock AS.
 */
package uk.co.gencoreoperative.frp;

import java.text.MessageFormat;

/**
 * Demonstrates the class hierarchy support
 */
public class HelloAdvancedWorld {
    public static void main(String... args) {
        Signals signals = new Signals();
        signals.register(Integer.class, new SignalListener<Integer>() {
            public void process(Integer integer, Signals signals) {
                System.out.println(MessageFormat.format("Integer {0}", integer));
            }
        });
        signals.register(Long.class, new SignalListener<Long>() {
            public void process(Long number, Signals signals) {
                System.out.println(MessageFormat.format("Long {0}", number));
            }
        });
        signals.register(Number.class, new SignalListener<Number>() {
            public void process(Number number, Signals signals) {
                System.out.println(MessageFormat.format("Number {0}", number));
            }
        });
        signals.add(new Integer(1));
        signals.add(new Long(2));
        signals.shutdownWhenComplete();
    }
}
