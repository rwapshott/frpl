package uk.co.gencoreoperative.frp;

/**
 * Demonstrates that if Signals fills up with requests, it will throttle the caller.
 */
public class ExpandingWorld {
    public static void main(String... args) {
        Signals signals = new Signals();
        signals.register(String.class, new SignalListener<String>() {
            public void process(String s, Signals signals) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(s);
            }
        });
        int ii = 0;
        while (signals.canAdd()) {
            ii++;
            String message = "String" + ii;
            System.out.println("Adding: " + message);
            signals.add(message);
        }

        signals.shutdownWhenComplete();
    }
}
