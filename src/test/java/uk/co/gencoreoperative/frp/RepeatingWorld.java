package uk.co.gencoreoperative.frp;

public class RepeatingWorld {
    public static void main(String... args) {
        Signals signals = new Signals();
        SignalFactory factory = new SignalFactory(signals);
        signals.register(String.class, new SignalListener<String>() {
            public void process(String s, Signals signals) {
                System.out.println(s);
            }
        });
        factory.repeats(500, 10, "Hello World");
        signals.shutdownWhenComplete();
    }
}
