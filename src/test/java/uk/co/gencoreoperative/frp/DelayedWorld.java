package uk.co.gencoreoperative.frp;

public class DelayedWorld {
    public static void main(String... args) {
        Signals s = new Signals();
        SignalFactory f = new SignalFactory(s);
        s.register(String.class, new SignalListener<String>() {
            public void process(String s, Signals signals) {
                System.out.println("Shutting down...");
                signals.shutdownWhenComplete();
            }
        });
        f.delayed(1000, "");
    }
}
