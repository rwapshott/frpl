package uk.co.gencoreoperative.frp;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Represents the central point in this FRP (Functional Reactive Programming) library.
 *
 * The concept of this library is based around a signal, which could be any object
 * and the listener which is associated with that signal. When the signal is added
 * to this class, its corresponding listener will trigger at some arbitrary but
 * timely point in the future.
 *
 * This is a model for asynchronous (happens-after) processing and flow control
 * of an application.
 *
 * Supports the concept of signal hierarchy. An Integer is also a Number. Both of
 * which could have signal listeners associated.
 *
 * Responsible for managing the detail around shutting down the framework.
 * Note: Once the caller has completed their processing, they should call
 * {@link #shutdownWhenComplete()} to indicate that the framework should shutdown.
 *
 * Thread Safety: This class is both thread safe, and intended for multi-threaded
 * access.
 */
public class Signals {
    private static final int QUEUE_SIZE = 1000;
    private Map<Class, Set<SignalListener>> map = new ConcurrentHashMap<Class, Set<SignalListener>>();
    private ExecutorService service = Executors.newFixedThreadPool(QUEUE_SIZE);
    private volatile boolean shutdown = false;
    private final AtomicInteger tasks = new AtomicInteger(0);
    private final Ender ender = new Ender();

    public Signals() {
        // To support more complex signals, we are listening and blocking on Runnable
        register(Runnable.class, new SignalListener<Runnable>() {
            public void process(Runnable runnable, Signals signals) {
                runnable.run();
            }
        });
    }

    /**
     * Register a signalListener with the framework. Listeners are invoked each time
     * a signal is provided which matches the class the SignalListener is intended for.
     *
     * @param signal The class of signal that should be responded to.
     * @param signalListener Non null signalListener to trigger.
     *
     * @param <T> Used to ensure the signal class and signalListener type match.
     */
    public <T> void register(final Class<T> signal, final SignalListener<T> signalListener) {
        if (!map.containsKey(signal)) {
            map.put(signal, new HashSet<SignalListener>());
        }
        map.get(signal).add(signalListener);
    }

    /**
     * Add a signal to the framework. If there is a registered listener, then
     * the signal will be queued to be processed.
     *
     * @param signal A non null signal.
     */
    public void add(final Object signal) {
        if (signal == null) throw new IllegalArgumentException();

        // Ignore non registered signals.
        final Class<?> signalClass = signal.getClass();
        if (!isValidSignal(signalClass)) return;

        // If the queue is full, pause the caller until there is space
        // Obviously do not delay in processing an End signal.
        int delay = 2;
        while (!End.class.isAssignableFrom(signalClass) && !canAdd()) {
            try {
                Thread.sleep(delay);
                delay *= 2;
            } catch (InterruptedException e) {
                if (Thread.interrupted()) return;
            }
        }

        tasks.incrementAndGet();

        // Generate task
        Runnable task = new Runnable() {
            public void run() {
                for (SignalListener listener : getListeners(signalClass)) {
                    try {
                        listener.process(signal, Signals.this);
                    } catch (Throwable e) {
                        System.err.println(MessageFormat.format("Unexpected error on Signal {0}: {1}",
                                signalClass.toString(), e.getMessage()));
                        e.printStackTrace();
                    }

                }
                tasks.decrementAndGet();

                if (shutdown && signal instanceof End) {
                    ender.process((End)signal, Signals.this);
                }
            }
        };

        // Process, ignore error if shutdown is in progress.
        try {
            service.execute(task);
        } catch (RejectedExecutionException e) {
            if (service.isShutdown()) return;
            throw e;
        }
    }

    /**
     * Indicates if Signals can accept another object to process.
     *
     * If too many objects have been added to Signals for processing, eventually it will
     * become full. At this point subsequent adds will cause the caller to be throttled.
     *
     * The caller can either automatically be throttled or wait an amount of time.
     *
     * Note: effort should be made to insure all listeners execute quickly.
     *
     * @return True if Signals can accept another task to the queue.
     */
    public boolean canAdd() {
        int total = tasks.get();
        return total < QUEUE_SIZE;
    }

    /**
     * Indicates if the Signal is valid.
     *
     * @param clazz Non null class to test.
     * @return True if the class will be processed.
     */
    private boolean isValidSignal(Class clazz) {
        if (clazz.isAssignableFrom(End.class)) return true;

        for (Class listenerClass : map.keySet()) {
            if (listenerClass.isAssignableFrom(clazz)) return true;
        }
        return false;
    }

    /**
     * Get all listeners that match the class.
     *
     * Will return empty collection for End signal.
     *
     * @param clazz Non null
     * @param <T> Enforces that the listeners returned are the same type as the class provided.
     *
     * @return Non null, possibly empty Set.
     */
    private <T> Set<SignalListener> getListeners(Class<T> clazz) {
        // The listener behaviour for the End signal is specific and handled by Ender.
        if (clazz.isAssignableFrom(End.class)) return Collections.emptySet();

        Set<SignalListener> listeners = new HashSet<SignalListener>();
        for (Class listenerClass : map.keySet()) {
            if (listenerClass.isAssignableFrom(clazz)) listeners.addAll(map.get(listenerClass));
        }
        return listeners;
    }

    /**
     * Indicates to the Signals that it should shutdown when all tasks have been
     * processed.
     */
    public void shutdownWhenComplete() {
        shutdown = true;
        add(new End());
    }

    /**
     * End represents a definite signal which the framework will monitor and use for shutdown.
     */
    private class End {
    }

    /**
     * Signal processor which will monitor the state of {@link Signals} and shut it down
     * once the task queue is empty.
     *
     * This listener will keep retrying the shutdown sequence until successful or interrupted.
     */
    private class Ender implements SignalListener<End> {
        public void process(End end, Signals signals) {
            if (tasks.get() == 0) {
                service.shutdown();
            } else {
                waitForRetry();
                signals.add(end);
            }
        }

        /**
         * Wait a small amount of time before re-queuing.
         */
        private void waitForRetry() {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                service.shutdown();
            }
        }
    }
}
