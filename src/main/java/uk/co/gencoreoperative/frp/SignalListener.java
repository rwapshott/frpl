package uk.co.gencoreoperative.frp;

/**
 * Represents a task processor in this framework. Each SignalListener will be registered
 * against a specific class.
 *
 * Listeners should aim to complete processing quickly, and if they generate subsequent
 * signals, they can do so with the provided Signals instance.
 */
public interface SignalListener<T> {
    /**
     * Process the given signal.
     *
     * @param t Non null signal.
     * @param signals Non null location to store subsequent signals.
     */
    void process(T t, Signals signals);
}
