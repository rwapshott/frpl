/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2015 ForgeRock AS.
 */
package uk.co.gencoreoperative.frp;

/**
 * Used for the generation of a variety signals with useful properties.
 */
public class SignalFactory {
    private final Signals signals;

    /**
     * @param signals Required target for the signals.
     */
    public SignalFactory(Signals signals) {
        this.signals = signals;
    }

    /**
     * Generates a repeating Signal which starts with the required delay, followed by the signal.
     * The assumption with this call is that the signal itself does not change between
     * repeated iterations.
     *
     * Note: This repeating sequence should not last forever, otherwise it will prevent
     * the Signals class from shutting down when requested.
     *
     * @param durationMillis Non negative duration in milliseconds delay.
     * @param count Number of times to repeat the signal.
     * @param signal The signal to repeat.
     */
    public void repeats(final int durationMillis, final int count, final Object signal) {
        signals.add(new Runnable() {
            public void run() {
                for (int ii = 0; ii < count; ii++) {
                    sleep(durationMillis);
                    signals.add(signal);
                }
            }
        });
    }

    /**
     * Delays by duration before sending the signal.
     * @param durationMillis Non negative duration in milliseconds.
     * @param signal The signal to send once delay has elapsed.
     */
    public void delayed(final int durationMillis, final Object signal) {
        signals.add(new Runnable() {
            public void run() {
                sleep(durationMillis);
                signals.add(signal);
            }
        });
    }

    private static void sleep(int durationMillis) {
        try {
            Thread.sleep(durationMillis);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
